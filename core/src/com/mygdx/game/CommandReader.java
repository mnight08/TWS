package com.mygdx.game;
import com.badlogic.gdx.Input.TextInputListener;


public class CommandReader implements TextInputListener {
	public String message;
   	@Override
   	public void input (String text) {
	   message=text;
	}

   	@Override
   	public void canceled () {
	}

   	public String getMessage(){
	   return message;
	}

	public CommandReader(){
	}
}

