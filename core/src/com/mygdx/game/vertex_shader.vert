//incoming Position attribute from our mesh
attribute vec3 Position;

//the transformation matrix of our mesh
uniform mat4 u_projView;

void main() {
    //transform our 3D world space position into homogeneous coordinates
    gl_Position = u_projView * vec4(Position, 1.0);
}

