package com.mygdx.game;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

public class DesktopLauncher{
	 public static void main (String[] argv) {
         
		 new LwjglApplication(new Game(), "Water Simulation", 1024, 768);               
 }
}

